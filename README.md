# Trillo Digital Website

## Description

Trillo Digital is a company specializing in app development. This static website serves as our online presence, showcasing our services and expertise in the industry. 

## Features

- Information about our services
- Details about our app development process
- Contact information

## Usage

To view the website, simply navigate to [http://trillo-digital-website.s3-website.ap-south-1.amazonaws.com](http://trillo-digital-website.s3-website.ap-south-1.amazonaws.com).

## Contributing

As this is a company website, we are not currently accepting contributions. However, feedback is always welcome. Please contact us with any suggestions or issues.

## Contact

For any inquiries, please contact us at [contact@trillodigital.com](mailto:contact@trillodigital.com).